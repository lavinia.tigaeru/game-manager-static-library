//
//  GameManager.swift
//  GameManager
//
//  Created by Lavinia Tigaeru on 26.01.2023.
//

public class GameManager {
    public class func generateCardSet(from symbols: [String]) -> [String] {
        var cardSet = symbols + symbols
        cardSet.shuffle()
        return cardSet
    }
    
    public class func computeScore(movesCount: Int, cardsCount: Int, timeLeft: Int) -> Int {
        var score = 0
        
        if movesCount > cardsCount * 3 {
            score += MatchProficiency.consolationPrize.rawValue
        } else if movesCount > cardsCount * 2 {
            score += MatchProficiency.poor.rawValue
        } else if movesCount > 3 * cardsCount / 2 {
            score += MatchProficiency.average.rawValue
        } else if movesCount > cardsCount {
            score += MatchProficiency.good.rawValue
        } else if movesCount == cardsCount {
            score += MatchProficiency.perfect.rawValue
        }
        
        score += timeLeft * Constants.pointsPerSecond
        
        return score
    }
}

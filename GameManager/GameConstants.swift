//
//  GameConstants.swift
//  GameManager
//
//  Created by Lavinia Tigaeru on 26.01.2023.
//

import Foundation

enum MatchProficiency: Int {
    case perfect = 100
    case good = 75
    case average = 50
    case poor = 25
    case consolationPrize = 10
}

struct Constants {
    static let pointsPerSecond = 10
}
